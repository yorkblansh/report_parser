import { Request as IRequest } from 'express';
import { Response as IResponse } from 'express';

class Adminka_Controller {
   public static show(req: IRequest, res: IResponse): void {
      console.dir(req.cookies);

      if (req.cookies.admin_key) {
         console.log('IN PARAM YES');
         //затем кидаем дополнительные куки для админа и отдаем статику
         res.sendFile(__dirname + '/build/index.html');
      } else {
         res.redirect('/');
      }
   }
}

export { Adminka_Controller };
